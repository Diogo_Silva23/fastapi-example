from fastapi import APIRouter, HTTPException

router = APIRouter()

@router.get("/")
async def home():
    return {"msg": "Hello World"}