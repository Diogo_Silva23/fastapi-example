from fastapi import APIRouter, HTTPException
from app.schemas.item import Item

router = APIRouter()

@router.get("/")
async def read_items():
    return [{"name": "Item Foo"}, {"name": "item Bar"}]

@router.get("/{item_id}")
async def read_item(item_id: str):
    return {"name": "Fake Specific Item", "item_id": item_id}

@router.put("/{item_id}", responses={403: {"description": "Operation forbidden"}},)
async def update_item(item_id: int, item: Item):
    if item_id != "foo":
        raise HTTPException(status_code=403, detail="You can only update the item: foo")
    return {"item_id": item_id, "name": "The Fighters"}
