from typing import List

from fastapi import FastAPI, Depends, Header, HTTPException
from sqlalchemy.orm import Session

from app.services import items, users, home
from app.config.database import SessionLocal, engine
from app.models.item import *
from app.models.user import *
from app.schemas.item import *
from app.schemas.user import *

import uvicorn

Base.metadata.create_all(bind=engine)

app = FastAPI()

app.include_router(home.router)

#Exemplo com tags nos decorators
app.include_router(users.router, tags=["users"])

#Exemplo sem tags nos decorators
app.include_router(items.router, prefix="/items", tags=["items"])

if __name__ == "__main__":
    uvicorn.run(app, host='0.0.0.0', port=8000)
    